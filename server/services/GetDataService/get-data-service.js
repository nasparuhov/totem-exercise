const request = require('request');
const authentication = require('../AuthenticationService/authentication-service');
const debug = require('debug')('server:data');

let service = this;

service.getArtists = (searchingName, limit, offset) => {
  const options = {
    url: 'https://api.spotify.com/v1/search',
    method: 'GET',
    headers: {
      Authorization: 'Bearer ' + authentication.getAccessToken()
    },
    qs: {
      q: searchingName ? searchingName : 'example',
      type: 'artist',
      limit: limit ? limit : 20,
      offset: offset ? offset : 0
    }
  }

  return new Promise(function(resolve, reject) {
    request(options, (error, response, body) => {
      if (!error && response.statusCode == 200) {
        resolve(JSON.parse(body).artists);
      } else {
        debug(`${error} \nStatus code is: ${response.statusCode}`);
        reject({data: `There is error with searching artist: ${error}`, statusCode: `Status code is: ${response.statusCode}`});
      }
    });
  });
}

service.getAlbums = (artistId, albumType, limit, offset) => {
  const options = {
    url: `https://api.spotify.com/v1/artists/${artistId}/albums`,
    method: 'GET',
    headers: {
      Authorization: 'Bearer ' + authentication.getAccessToken()
    },
    qs: {
      album_type: albumType ? albumType : 'album',
      limit: limit ? limit : 20,
      offset: offset ? offset : 0
    }
  }

  return new Promise(function(resolve, reject) {
    request(options, (error, response, body) => {
      if (!error && response.statusCode == 200) {
        resolve(JSON.parse(body));
      } else {
        debug(`${error} \nStatus code is: ${response.statusCode}`);
        reject({data: `There is error with searching artist: ${error}`, statusCode: `Status code is: ${response.statusCode}`});
      }
    });
  });
}

service.getSongs = (albumId, limit, offset) => {
  const options = {
    url: `https://api.spotify.com/v1/albums/${albumId}/tracks`,
    method: 'GET',
    headers: {
      Authorization: 'Bearer ' + authentication.getAccessToken()
    },
    qs: {
      limit: limit ? limit : 20,
      offset: offset ? offset : 0
    }
  }

  return new Promise(function(resolve, reject) {
    request(options, (error, response, body) => {
      if (!error && response.statusCode == 200) {
        resolve(JSON.parse(body));
      } else {
        debug(`${error} \nStatus code is: ${response.statusCode}`);
        reject({data: `There is error with searching artist: ${error}`, statusCode: `Status code is: ${response.statusCode}`});
      }
    });
  });
}

service.getSuggestions = (searchingName) => {
  const options = {
    url: 'https://api.spotify.com/v1/search',
    method: 'GET',
    headers: {
      Authorization: 'Bearer ' + authentication.getAccessToken()
    },
    qs: {
      q: searchingName ? searchingName : 'example',
      type: 'artist',
      limit: 20,
      offset: 0
    }
  }
  return new Promise(function(resolve, reject) {
    request(options, (error, response, body) => {
      if (!error && response.statusCode == 200) {
        let suggestions = JSON.parse(body).artists.items.map(artist => artist.name)
        resolve(suggestions);
      } else {
        debug(`${error} \nStatus code is: ${response.statusCode}`);
        reject({data: `There is error with searching artist: ${error}`, statusCode: `Status code is: ${response.statusCode}`});
      }
    });
  });
}

module.exports = service;
