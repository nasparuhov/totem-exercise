const request = require('request');
const debug = require('debug')('server:authenticate')

let service = this;
service.access_token = '';

let options = {
  url: 'https://accounts.spotify.com/api/token',
  method: 'POST',
  headers: {
    Authorization: 'Basic MzJmODRkOTk3ZjdmNDI5MWI5NDc2NTcxMzVkZmNkYmI6NDk1Y2M2NjA5ZmU1NGE3MmExYzMzMTViNWE0OWM2MGY='
  },
  form: {
    grant_type: 'client_credentials'
  }
}

service.generateAccessToken = () => {
  return new Promise(function (resolve, reject) {
    request(options, (error, response, body) => {
      if (!error && response.statusCode == 200) {
        let info = JSON.parse(body);
        service.access_token = info.access_token;
        resolve(info.access_token);
      } else {
        debug(`${error} \nStatus code is: ${response.statusCode}`);
        reject(error);
        setTimeout(service.generateAccessToken, 1000);
      }
    });
  });
}

service.getAccessToken = () => {
  return service.access_token;
}

module.exports = service;
