const express = require('express');
const path = require('path');
const bodyParser = require('body-parser');
const cors = require('cors');
const authentication = require('./services/AuthenticationService/authentication-service');
const debug = require('debug')('server:app');

const search = require('./routes/search');
const artists = require('./routes/artists');
const album = require('./routes/album');
const index = require('./routes/index');
const suggestion = require('./routes/suggestion');

const app = express();

app.use(cors());

app.use(bodyParser.json());
app.use(express.static(path.resolve('public')));
app.use(bodyParser.urlencoded({ extended: false }));

app.use('/api', search);
app.use('/api/artist', artists);
app.use('/api/album', album);
app.use('/api/suggestion', suggestion);
app.use('*', index);

authentication.generateAccessToken().then(token => {
  setInterval(authentication.generateAccessToken, 3000000);
  debug(`Access token for spotify: ${token}`);
}).catch(e => {
  debug(`There is error with getting token: ${e}`);
});

module.exports = app;
