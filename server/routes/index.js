const router = require('express').Router();
const path = require('path');

/* GET home page. */
router.get('/', function(req, res, next) {
  res.sendFile(path.resolve('public', 'index.html'));
});

module.exports = router;
