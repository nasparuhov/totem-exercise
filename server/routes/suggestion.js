const router = require('express').Router();
const getData = require('../services/GetDataService/get-data-service');
const debug = require('debug')('server:suggestions')

/* GET home page. */
router.get('/', function(req, res, next) {
  if (!req.query.q) {
    res.send('Search name is required.');
  } else {
    getData.getSuggestions(req.query.q).then(result => {
      res.send(result)
    }).catch(e => {
      res.send(`There is error with getting suggestions: ${e}`);
    });
  }
});

module.exports = router;
