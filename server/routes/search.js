const router = require('express').Router();
const getData = require('../services/GetDataService/get-data-service');
const debug = require('debug')('server:search');

/* GET home page. */
router.get('/search', function(req, res, next) {
  if (!req.query.q) {
    res.send('Search name is required.');
  } else {
    getData.getArtists(req.query.q, req.query.limit, req.query.offset).then(result => {
      res.send(result);
    }).catch(e => {
      authentication.generateAccessToken().then(token => {
        getData.getArtists(req.query.q, req.query.limit, req.query.offset).then(result => {
          res.send(result);
        }).catch(e => {
          res.send(e);
        });
      }).catch(e => {
        res.send(`There is error with getting token: ${e}`);
      });
    });
  }
});

module.exports = router;
