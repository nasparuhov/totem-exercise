const router = require('express').Router();
const getData = require('../services/GetDataService/get-data-service')

/* GET home page. */
router.get('/:id', function(req, res, next) {
  if (!req.params.id) {
    res.send('Artist ID is required.');
  } else {
    getData.getAlbums(req.params.id, req.query.album_type, req.query.limit, req.query.offset).then(result => {
      res.send(result);
    }).catch(e => {
      authentication.generateAccessToken().then(token => {
        getData.getAlbums(req.params.id, req.query.album_type, req.query.limit, req.query.offset).then(result => {
          res.send(result);
        }).catch(e => {
          res.send(e);
        });
      }).catch(e => {
        res.send(`There is error with getting token: ${e}`);
      });
    });
  }
});

module.exports = router;
