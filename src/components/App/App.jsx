import React from 'react';
import { BrowserRouter as Router, Redirect, Route, Switch } from 'react-router-dom';
import ArtistsPage from '../Artists/ArtistsPage';
import AlbumPage from '../Album/AlbumPage';
import SongsPage from '../Songs/SongsPage';
import NavBar from '../NavBar/NavBar';
import Footer from '../Footer/Footer';
import './App.scss';

export default class App extends React.Component {
  constructor(props) {
    super(props);
  }
  render() {
    return (
      <div>
        <Router>
          <div className="App-container">
            <NavBar />
            <div className="App-content">
              <Switch>
                <Route exact path='/' component={ArtistsPage} />
                <Route path='/album' component={AlbumPage} />
                <Route path='/songs' component={SongsPage} />
                <Redirect to='/' />
              </Switch>
            </div>
            <Footer />
          </div>
        </Router>
      </div>
    );
  }
}
