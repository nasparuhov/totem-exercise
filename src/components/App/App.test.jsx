import React from 'react'
import { shallow, configure } from 'enzyme';
import Adapter from 'enzyme-adapter-react-15.4';
import App from './App.jsx'
import { Redirect, Route } from 'react-router-dom';
import ArtistsPage from '../Artists/ArtistsPage';
import AlbumPage from '../Album/AlbumPage';
import SongsPage from '../Songs/SongsPage';

configure({ adapter: new Adapter() });

describe('App Component rendering', () => {

  it('should render App component without errors', () => {
    expect(shallow(<App />).exists()).toBe(true)
  })

  it('should render ArtistPage route', () => {
    expect(shallow(<App />).find(Route).find({ path: '/', component: ArtistsPage }).length).toBe(1)
  })

  it('should render AlbumPage route', () => {
    expect(shallow(<App />).find(Route).find({component: AlbumPage, path: '/album'}).length).toBe(1)
  })

  it('should render SongsPage route', () => {
    expect(shallow(<App />).find(Route).find({component: SongsPage, path: '/songs'}).length).toBe(1)
  })

  it('should render Redirect route', () => {
    expect(shallow(<App />).find(Redirect, { to: '/' }).length).toBe(1)
  })
 })
