import React, { Component } from 'react';
import Suggestions from './Suggestions';
import PropTypes from 'prop-types';
import axios from 'axios';
import { debounce } from 'throttle-debounce';
import './SearchBar.scss';

export default class SearchBar extends Component {
  static propTypes = {
    label: PropTypes.string,
    placeholder: PropTypes.string,
    searchHandler: PropTypes.func,
    clearSearch: PropTypes.func
  }

  static defaultProps = {
    label: '',
    placeholder: 'Search for an artist'
  }

  constructor(props, defaultProps) {
    super(props, defaultProps);
    this.state = {
      searchTerm: '',
      suggestions: [],
      timeout: {},
      selectedSuggestion: null,
      searchComplete: false
    };
    this.receiveSuggestions = debounce(700, this.receiveSuggestions);
  }

  handleChange = input => {
    let self = this;
    let text = input.target.value.trim();
    this.setState({ searchTerm: text, searchComplete: false });
    if (text.length === 0) {
      this.setState({ suggestions: [], selectedSuggestion: null });
    } else {
      self.receiveSuggestions(text);
    }
  };

  receiveSuggestions = input => {
    if (!input.length) {
      return;
    } else {
      let self = this;
      axios(`http://127.0.0.1:3000/api/suggestion?q=${input}`).then(response => {
        if (self.state.searchTerm.length && !this.state.searchComplete) {
          self.setState({ suggestions: response.data });
        }
      }).catch(e => {
        alert(`There is error gtting the suggestions: ${e}`);
      });
    }
  }

  selectItem = selected => {
    this.setState({ searchTerm: selected, suggestions: [] }, () => {
      this.handleSearch();
    });
  }

  onBlur = e => {
    let currentTarget = e.currentTarget;

    setTimeout(() => {  //Without the timeout it just recieve the input box as active element
      if (!currentTarget.contains(document.activeElement)) {
        this.setState({ suggestions: [] });
      }
    }, 0);
  }

  handleKeyDown = event => {
    switch (event.key) {
      case 'ArrowDown': {
        event.preventDefault();
        if (this.state.selectedSuggestion === null) {
          this.setState({ selectedSuggestion: 0 });
          break;
        }
        if (0 <= this.state.selectedSuggestion && this.state.selectedSuggestion < this.state.suggestions.length - 1) {
          this.setState({ selectedSuggestion: this.state.selectedSuggestion + 1 });
        }
        break;
      }
      case 'ArrowUp': {
        event.preventDefault();
        if (0 < this.state.selectedSuggestion && this.state.selectedSuggestion < this.state.suggestions.length) {
          this.setState({ selectedSuggestion: this.state.selectedSuggestion - 1 });
        }
        break;
      }
      case 'Enter': event.preventDefault(); this.handleSearch(this.state.suggestions[this.state.selectedSuggestion]); break;
      default: break;
    }
  }

  handleSearch = serchInput => {
    this.props.searchHandler(serchInput ? serchInput : this.state.searchTerm);
    this.setState({
      suggestions: [],
      selectedSuggestion: 0,
      searchTerm: serchInput ? serchInput : this.state.searchTerm,
      searchComplete: true
    });
  }

  clearSearch = () => {
    this.setState({
      searchTerm: '',
      suggestions: [],
      selectedSuggestion: null
    });
    this.props.clearSearch();
  }

  render() {
    return (
      <div className='search-bar-container' tabIndex="1" onBlur={this.onBlur}>
        <div className='search-bar-label'>{this.props.label}</div>
        <div className='form-group search-bar-field'>
          <span className="input-group">
            <input autoFocus type="text" value={this.state.searchTerm} placeholder={this.props.placeholder} className='formControl search-bar-input' onChange={this.handleChange} onKeyDown={this.handleKeyDown} />
            {this.state.searchTerm.length > 0 ? (<span className="input-group-btn">
              <button type="button" className='search-bar-button-clear' onClick={this.clearSearch}></button>
            </span>) : null}
            <span className="input-group-btn">
              <button type="button" className='search-bar-button-search' onClick={this.handleSearch}></button>
            </span>
          </span>
        </div>
        <Suggestions
          searchTerm={this.state.searchTerm}
          selectedSuggestion={this.state.selectedSuggestion}
          suggestions={this.state.suggestions}
          selectItem={this.selectItem} />
      </div>
    );
  }
}
