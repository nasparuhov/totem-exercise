import React from 'react';
import { mount, shallow, configure } from 'enzyme';
import sinon from 'sinon';
import Adapter from 'enzyme-adapter-react-15.4';
import moxios from 'moxios';
import axios from 'axios';
import SearchBar from './SearchBar';

configure({ adapter: new Adapter() });

jest.useFakeTimers();

describe('SearchBar render', () => {
  it('should render SearchBar component without errors', () => {
    const searchBar = mount(<SearchBar placeholder="ExamplePlaceholder" />);
    expect(searchBar.exists()).toEqual(true);
  })
})

describe('SearchBar search and get results', () => {
  it('should change the searchTerm state on search for something', () => {
    const searchBar = mount(<SearchBar />);
    searchBar.find('input').simulate('change', { target: { value: 'eminem' } });
    jest.runOnlyPendingTimers();
    //expect(searchBar.state().searchTerm).toEqual('eminem');
  })

  it('should clear the searchTerm state and suggestions on clear the input', () => {
    const searchBar = mount(<SearchBar />);
    searchBar.setState({ suggestions: ['asd', 'wba'] });
    searchBar.find('input').simulate('change', { target: { value: '' } });
    expect(searchBar.state().searchTerm).toEqual('');
    expect(searchBar.state().suggestions).toEqual([]);
  })

  it('should search for something on press Enter key', () => {
    let handleSearch = sinon.spy()
    const searchBar = mount(<SearchBar searchHandler={handleSearch} />);
    searchBar.setState({ searchTerm: 'example' });
    searchBar.setState({ suggestions: ['asd', 'wba'] });
    searchBar.find('input').simulate('keyDown', { key: 'Enter' });
    expect(searchBar.state().suggestions).toEqual([]);
    expect(handleSearch.calledOnce).toBeTruthy();
  })

  it('should not search for something on press any key rather then Enter', () => {
    let handleKeyPress = (input) => {
      expect(input.key).toBe('someKey');
    }
    let handleSearch = sinon.spy();
    const searchBar = mount(<SearchBar onKeyPress={handleKeyPress} searchHandler={handleSearch} />);
    searchBar.find('input').simulate('keyPress', { key: 'someKey' });
    expect(handleSearch.callCount).toBe(0);
  })
})

describe('Search for items into searchBar', () => {
  const searchBar = shallow(<SearchBar />);
  global.alert = sinon.spy();

  beforeEach(function () {
    moxios.install()
  })

  afterEach(function () {
    moxios.uninstall()
  })

  it('should serach for suggestions', () => {
    const mockResponse = ['1', '2', '3', '4', '5'];
    searchBar.instance().receiveSuggestions('example');
    jest.runOnlyPendingTimers();
    moxios.wait(function () {
      let request = moxios.requests.mostRecent();
      console.log(request);
      // request.respondWith({
      //   status: 200,
      //   response: mockResponse
      // }).then(function () {
      //   artistPage.update();
      //   expect(searchBar.state().suggestions).toBe(mockResponse);
      //   done();
      // });
    });
    searchBar.unmount();
  })
})

describe('SearchBar selectItem and clear search events', () => {
  it('should clear the serchTerm state on SearchBar', () => {
    const clearSearch = jest.fn();
    const searchBar = mount(<SearchBar placeholder="ExamplePlaceholder" clearSearch={clearSearch} />);
    searchBar.setState({ searchTerm: 'example' });
    searchBar.find('button.search-bar-button-clear').simulate('click')
    expect(searchBar.state().searchTerm).toEqual('');
  })

  it('should trigger onBlur event on SearchBar input', () => {
    const searchBar = mount(<SearchBar placeholder="ExamplePlaceholder" />);
    searchBar.setState({ suggestions: ['example', 'example2', 'example3'] });
    searchBar.find('input').simulate('blur', { currentTarget: { contains: () => false } });
    jest.runOnlyPendingTimers();
    expect(searchBar.state().suggestions).toEqual([]);
  })

  it('should click on suggestion and put in into searchTerm state on SearchBar', () => {
    const searchHandler = sinon.spy();
    const searchBar = mount(<SearchBar placeholder="ExamplePlaceholder" searchHandler={searchHandler} />);
    searchBar.setState({ suggestions: ['asd', 'wba', 'example', 'example5'] });
    searchBar.find('span.search-bar-suggestions-item').at(2).simulate('click');
    expect(searchBar.state().searchTerm).toEqual('example');
    expect(searchBar.state().suggestions).toEqual([]);
    expect(searchHandler.calledOnce).toBeTruthy();
  })
})
