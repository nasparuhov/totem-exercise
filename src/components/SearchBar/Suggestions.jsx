import React from 'react';
import PropTypes from 'prop-types';

export default class Suggestions extends React.Component {
  static propTypes = {
    selectedSuggestion: PropTypes.number,
    suggestions: PropTypes.array,
    selectItem: PropTypes.func,
    searchTerm: PropTypes.string
  };

  constructor(props) {
    super(props);
  }


  componentDidUpdate() {
    if (this.activeElement) {
      this.activeElement.scrollIntoView({
        behavior: 'smooth',
        block: 'end',
        inline: 'center'
      });
    }
  }

  render() {
    return (
      <div>
        {this.props.suggestions.length !== 0 ? (<div className='search-bar-suggestions-container'>
          {this.props.suggestions.map((suggestion, index) => {
            return (
              <span
                key={suggestion + Math.random()}
                className={'search-bar-suggestions-item ' +
                  (this.props.selectedSuggestion === index ? 'active' : '')}
                onClick={this.props.selectItem.bind(this, suggestion)}
                ref={node => {
                  if (this.props.selectedSuggestion === index) {
                    this.activeElement = node;
                  }
                }} >
                  <strong>{suggestion.substr(0, this.props.searchTerm.length)}</strong>
                  <span>{suggestion.substr(this.props.searchTerm.length)}</span>
              </span>
            );
          })}
        </div>) : ''}
      </div>
    );
  }
}
