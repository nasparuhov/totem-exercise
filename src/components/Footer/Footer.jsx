import React from 'react';

const Footer = () => {
  return (
    <footer className='footer'>
      &copy; 2017 - Totem by Ingima
    </footer>
  );
};

export default Footer;

