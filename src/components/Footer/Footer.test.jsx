import React from 'react'
import { shallow, configure } from 'enzyme';
import Adapter from 'enzyme-adapter-react-15.4';
import Footer from './Footer.jsx'

configure({ adapter: new Adapter() });

describe('Footer Component rendering', () => {

  it('should render Footer component with footer element', () => {
    expect(shallow(<Footer />).find('footer').length).toBe(1)
  })
 })
