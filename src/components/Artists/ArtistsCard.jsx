import React from 'react';
import { Link } from 'react-router-dom';
import PropTypes from 'prop-types';
import noArtistImage from './../../assets/images/noArtistImage.png';

export default class ArtistCard extends React.Component {
  static propTypes = {
    artist: PropTypes.shape({
      id: PropTypes.string,
      images: PropTypes.array,
      name: PropTypes.string,
      genres: PropTypes.array,
      popularity: PropTypes.number,
      followers: PropTypes.shape({
        total: PropTypes.number
      })
    })
  };

  constructor(props) {
    super(props);
  }

  render() {
    return (
      <div className='media artist-card-container' key={this.props.artist.id}>
        <Link to={{
          pathname: '/album',
          artist: JSON.stringify(this.props.artist)
        }}>
          <div className='media-left'>
            <img className='media-object media artist-card-image'
              src={this.props.artist.images.length ? this.props.artist.images.pop().url : noArtistImage} />
          </div>
          <div className='media-body media artist-card-content'>
            <h2 className='media-heading media artist-card-name'>{this.props.artist.name}</h2>
            <h4 className='media-heading artist-card-description'>Genres: {this.props.artist.genres.length ? this.props.artist.genres.map(genre => {
              return (
                <span key={genre} className='media artist-card-description-sm'>{genre + (genre !== this.props.artist.genres.slice(-1).pop() ? ', ' : '')}</span>
              );
            }) : (<span className='media artist-card-description-sm'>none</span>)}</h4>
            <h4 className='media-heading artist-card-description'>Popularity: <span className='artist-card-description-sm'>{this.props.artist.popularity}</span></h4>
            <h4 className='media-heading artist-card-description'>Followers: <span className='artist-card-description-sm'>{this.props.artist.followers.total}</span></h4>
          </div>
        </Link>
      </div>
    );
  }
}
