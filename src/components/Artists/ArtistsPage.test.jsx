import React from 'react';
import { mount, shallow, configure } from 'enzyme';
import sinon from 'sinon';
import Adapter from 'enzyme-adapter-react-15.4';
import Pagination from './../Pagination/Pagination';
import ArtistsPage from './ArtistsPage';
import ArtistsList from './ArtistsList';
import moxios from 'moxios';

configure({ adapter: new Adapter() });

describe('ArtistsPage Component rendering', () => {
  const artistPage = shallow(<ArtistsPage />);
  it('should render ArtistsPage component without errors', () => {
    expect(artistPage.exists()).toBe(true)
  })
})

describe('ArtistsPage searchForArtist method with only 1 page returned', () => {
  const artistPage = shallow(<ArtistsPage />);
  const mockResponse = {
    "items": [{
      "external_urls": {
        "spotify": "https://open.spotify.com/artist/08td7MxkoHQkXnWAYD8d6Q"
      },
      "genres": [],
      "id": "08td7MxkoHQkXnWAYD8d6Q",
      "images": [{}, {
        "url": "https://i.scdn.co/image/b414091165ea0f4172089c2fc67bb35aa37cfc55"
      }],
      "name": "Tania Bowra",
      "popularity": 0
    },
    {
      "external_urls": {
        "spotify": "https://open.spotify.com/artist/08td7MxkoHQkXnWAYD8d6Q"
      },
      "genres": [],
      "id": "08td7MxkoHQkXnWA",
      "images": [{}, {
        "url": "https://i.scdn.co/image/b414091165ea0f4172089c2fc67bb35aa37cfc55"
      }],
      "name": "Tania Bowra",
      "popularity": 0
    }],
    "total": 1
  }

  beforeEach(function () {
    moxios.install()
  })

  afterEach(function () {
    moxios.uninstall()
  })

  it('should render ArtistsPage component without errors', () => {
    expect(artistPage.exists()).toBe(true)
  })

  it('should render 1 page with 2 artists', (done) => {
    global.alert = sinon.spy();
    artistPage.instance().searchForArtist('eminem');
    moxios.wait(function () {
      let request = moxios.requests.mostRecent()
      request.respondWith({
        status: 200,
        response: mockResponse
      }).then(function () {
        artistPage.update();
        expect(artistPage.state().listOfArtists).toBe(mockResponse);
        expect(artistPage.find(ArtistsList).props().listOfArtists).toBe(mockResponse.items);
        done();
      });
    });
  })

})
