import React from 'react';
import SearchBar from '../SearchBar/SearchBar';
import Pagination from './../Pagination/Pagination';
import ArtistsList from './ArtistsList';
import { Well } from 'react-bootstrap';
import axios from 'axios';
import './ArtistsPage.scss';

export default class ArtistsPage extends React.Component {
  constructor(props) {
    super(props);
    this.state = {
      listOfArtists: {
        items: [],
        total: 1
      },
      searchedArtist: '',
      currentPage: 1,
      totalPages: 1,
      noResult: false
    };
  }

  searchForArtist = artistName => {
    let self = this;

    if (!artistName.length) {
      return;
    }

    this.setState({ searchedArtist: artistName });

    let queryString = new URLSearchParams();
    let params = {
      q: artistName,
      limit: 10,
      offset: (this.state.currentPage - 1) * 10
    };
    for (let key in params) {
      if ({}.hasOwnProperty.call(params, key)) {
        queryString.append(key, params[key]);
      }
    }

    axios('http://127.0.0.1:3000/api/search?' + queryString.toString())
    .then(response => {
      self.setState({ listOfArtists: response.data });
      self.setState({ totalPages: Math.ceil(response.data.total / 10) });
      response.data.items.length ? this.setState({ noResult: false }) : this.setState({ noResult: true });
    }).catch(error => {
      alert(error);
    });
  }

  handlePageChange = pageNumber => {
    this.setState({ currentPage: pageNumber }, () => {
      this.searchForArtist(this.state.searchedArtist);
    });
  }

  clearSearch = () => {
    this.setState({ listOfArtists: { items: [] }, totalPages: 1 });
  }

  render() {
    return (
      <div className='container'>
        <ol className='breadcrumb'>
          <li className='active'>Recherche</li>
        </ol>
        <div className='page-header'>
          <h1>Artistes</h1>
        </div>
        <SearchBar
          searchHandler={this.searchForArtist}
          placeholder='Rechercher un artiste Spotify'
          label=''
          clearSearch={this.clearSearch}/>
        <div className='artist-list'>
          {this.state.noResult ? (<Well className='text-center'>No results found from this search. Plese use the suggestions on tiping</Well>) : ''}
          <ArtistsList listOfArtists={this.state.listOfArtists.items}/>
        </div>
        <Pagination totalPages={this.state.totalPages} handlePageChange={this.handlePageChange} />
      </div>
    );
  }
}
