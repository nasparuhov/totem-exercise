import React from 'react';
import PropTypes from 'prop-types';
import ArtistsCard from './ArtistsCard';

export default class ArtistList extends React.Component {
  static propTypes = {
    listOfArtists: PropTypes.array
  }

  constructor(props) {
    super(props);
  }

  render() {
    return (
      <div>
        {this.props.listOfArtists.map(artist => {
          return (
            <div key={artist.id}>
              <ArtistsCard artist={artist} />
            </div>
          );
        })}
      </div>
    );
  }
}
