import React from 'react'
import { shallow, configure } from 'enzyme';
import Adapter from 'enzyme-adapter-react-15.4';
import NavBar from './NavBar.jsx'
import { Link } from 'react-router-dom';

configure({ adapter: new Adapter() });

describe('NavBar Component rendering', () => {

  it('should render NavBar component without errors', () => {
    expect(shallow(<NavBar />).find('nav.navbar').exists()).toBe(true)
  })

  it('should have Lint into NavBar component', () => {
    expect(shallow(<NavBar />).find(Link).find({ to: '/' }).length).toBe(1)
  })
 })
