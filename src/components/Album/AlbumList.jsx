import React from 'react';
import PropTypes from 'prop-types';
import AlbumCard from './AlbumCard';

export default class AlbumList extends React.Component {
  static propTypes = {
    listOfAlbums: PropTypes.array,
    artist: PropTypes.object
  }

  constructor(props) {
    super(props);
  }

  render() {
    return (
      <div className='album-container'>
        {this.props.listOfAlbums.map(album => {
          return (
            <div key={album.id}>
              <AlbumCard album={album} artist={this.props.artist}/>
            </div>
          );
        })}
      </div>
    );
  }
}
