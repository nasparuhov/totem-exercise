import React from 'react';
import { Link } from 'react-router-dom';
import PropTypes from 'prop-types';

export default class ArtistCard extends React.Component {
  static propTypes = {
    album: PropTypes.shape({
      id: PropTypes.string,
      images: PropTypes.array,
      name: PropTypes.string
    }),
    artist: PropTypes.object
  };

  constructor(props) {
    super(props);
  }

  render() {
    return (
      <div className='col-xs-12 col-sm-6 col-md-4 col-lg-3'>
        <Link to={{
          pathname: '/songs',
          artist: JSON.stringify(this.props.artist),
          album: JSON.stringify(this.props.album)
        }}>
          <div className='thumbnail text-center album-content'>
            <img src={this.props.album.images[1] ? this.props.album.images[1].url : 'http://placehold.it/255x255'} className='album-image' />
            <div className='caption album-name-container'>
              <h4 className='album-name-text'>{this.props.album.name}</h4>
            </div>
          </div>
        </Link>
      </div>
    );
  }
}
