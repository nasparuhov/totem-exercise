import React from 'react';
import PropTypes from 'prop-types';
import { Well } from 'react-bootstrap';
import Pagination from './../Pagination/Pagination';
import axios from 'axios';
import AlbumList from './AlbumList';
import './AlbumPage.scss';

export default class AlbumPage extends React.Component {
  static propTypes = {
    location: PropTypes.shape({
      artist: PropTypes.string
    })
  }

  constructor(props) {
    super(props);
    this.state = {
      artist: this.props.location.artist ? JSON.parse(this.props.location.artist) : JSON.parse(sessionStorage.getItem('artist')),
      albums: {
        items: []
      },
      currentPage: 1,
      totalPages: 1
    };
  }

  componentWillMount() {
    if (this.props.location.artist) {
      sessionStorage.setItem('artist', this.props.location.artist);
    }
    this.searchForAlbums();
  }

  searchForAlbums = () => {
    let self = this;
    let offset = (this.state.currentPage - 1) * 8;
    axios(`http://127.0.0.1:3000/api/artist/${this.state.artist.id}?limit=8&offset=${offset}`)
      .then(response => {
        self.setState({ albums: response.data });
        self.setState({ totalPages: Math.ceil(response.data.total / 8) });
      }).catch(error => {
        alert(error);
      });
  }

  handlePageChange = index => {
    this.setState({ currentPage: index }, () => {
      this.searchForAlbums();
    });
  }

  renderPages = () => {
    let items = [];
    for (let number = 1; number <= this.state.totalPages; number++) {
      items.push(
        <Pagination.Item active={number === this.state.currentPage} key={number}
          onClick={this.handlePageClick.bind(this, number)}>{number}</Pagination.Item>
      );
    }
    return items;
  }

  render() {
    return (
      <div className='container'>
        <ol className='breadcrumb'>
          <li><a href='/'>Recherche</a></li>
          <li className='active'>{this.state.artist.name}</li>
        </ol>
        <div className='page-header' style={this.state.artist.id.length ? { display: 'block' } : { display: 'none' }}>
          <h1>Albums</h1>
          <div className='album-artist-container'>
            <h2 className='album-artist-name'>{this.state.artist.name}</h2>
            <a href={this.state.artist.external_urls.spotify} className='media-heading album-artist-external_url' target='_blank'>(view artist)</a>
          </div>
        </div>
        <div className='row album-list'>
          {!this.state.albums.items.length && this.state.artist.id ? (<Well className='text-center album-list-empty'>This artist has no albums</Well>) : ''}
          <AlbumList listOfAlbums={this.state.albums.items} artist={this.state.artist} />
        </div>
        <Pagination totalPages={this.state.totalPages} handlePageChange={this.handlePageChange} />
      </div>
    );
  }
}
