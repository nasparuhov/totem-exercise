import React from 'react';
import PropTypes from 'prop-types';
import SongsItem from './SongsItem';

export default class SongsList extends React.Component {
  static propTypes = {
    songs: PropTypes.array
  }

  constructor(props) {
    super(props);
  }

  render() {
    return (
      <div>
        <div>
          <ul className='list-group'>
            {this.props.songs.map(song => {
              song.duration_m = Math.floor((song.duration_ms / 60000));
              song.duration_s = Math.floor((song.duration_ms % 60000) / 1000);
              return (
                <SongsItem song={song} key={song.id} />
              );
            })}
          </ul>
        </div>
      </div>
    );
  }
}
