import React from 'react';
import PropTypes from 'prop-types';
import { Link } from 'react-router-dom';
import axios from 'axios';
import SongsPageHeader from './SongsPageHeader';
import SongsList from './SongsList';
import './SongsPage.scss';

class SongsPage extends React.Component {
  static propTypes = {
    location: PropTypes.shape({
      album: PropTypes.string,
      artist: PropTypes.string
    })
  }

  constructor(props, defaultProps) {
    super(props, defaultProps);
    this.state = {
      album: this.props.location.album ? JSON.parse(this.props.location.album) : JSON.parse(sessionStorage.getItem('album')),
      artist: this.props.location.artist ? JSON.parse(this.props.location.artist) : JSON.parse(sessionStorage.getItem('artist')),
      songs: {
        items: []
      }
    };
  }

  componentWillMount() {
    if (this.props.location.album) {
      sessionStorage.setItem('album', this.props.location.album);
    }
    this.searchForSongs();
  }

  searchForSongs = () => {
    let self = this;

    axios(`http://127.0.0.1:3000/api/album/${this.state.album.id}?limit=50`).then(response => {
      self.setState({ songs: response.data });
    }).catch(error => {
      alert(error);
    });
  }

  render() {
    return (
      <div className='container'>
        <ol className='breadcrumb'>
          <li><a href='/'>Recherche</a></li>
          <li>
            <Link to={{ pathname: '/album', artist: JSON.stringify(this.state.artist) }}>
              {this.state.artist.name}
            </Link>
          </li>
          <li className='active'>{this.state.album.name}</li>
        </ol>
        <SongsPageHeader artist={this.state.artist} album={this.state.album} />
        <SongsList songs={this.state.songs.items} />
      </div>
    );
  }
}

export default SongsPage;
