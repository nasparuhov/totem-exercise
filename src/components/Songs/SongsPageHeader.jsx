import React from 'react';
import PropTypes from 'prop-types';

export default class SongsPageHeader extends React.Component {
  static propTypes = {
    album: PropTypes.shape({
      external_urls: PropTypes.shape({
        spotify: PropTypes.string
      }),
      images: PropTypes.shape([
        {
          url: PropTypes.string
        }
      ]),
      name: PropTypes.string
    }),
    artist: PropTypes.shape({
      name: PropTypes.string,
      external_urls: PropTypes.shape({
        spotify: PropTypes.string
      }),
    })
  }

  constructor(props) {
    super(props);
  }

  render() {
    return (
      <div className='page-header songs-album-container'>
        <div className='row'>
          <div className='col-xs-12 col-sm-4 col-md-4 col-lg-3'>
            <a href={this.props.album.external_urls.spotify}>
              <img src={this.props.album.images[1] ? this.props.album.images[1].url : 'http://placehold.it/640x640'}
                className='songs-album-image' alt={'Album name'} />
            </a>
          </div>
          <div className='col-xs-12 col-sm-8 col-md-8 col-lg-9'>
            <a href={this.props.artist.external_urls.spotify}>
              <h1 className='songs-artist-name'>{this.props.artist.name}</h1>
            </a>
            <a href={this.props.album.external_urls.spotify}>
              <h2 className='songs-album-name'>{this.props.album.name}</h2>
            </a>
          </div>
        </div>
      </div>
    );
  }
}
