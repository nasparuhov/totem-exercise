import React from 'react';
import PropTypes from 'prop-types';

export default class SongsItem extends React.Component {
  static propTypes = {
    song: PropTypes.shape({
      id: PropTypes.string,
      name: PropTypes.string,
      track_number: PropTypes.number,
      duration_m: PropTypes.number,
      duration_s: PropTypes.number
    })
  }

  constructor(props) {
    super(props);
  }

  render() {
    return (
      <li className='list-group-item' key={this.props.song.id}>
        {this.props.song.track_number}. {this.props.song.name}
        <span className='badge'>{this.props.song.duration_m}:{(this.props.song.duration_s < 10 ? '0' : '') + this.props.song.duration_s}</span>
      </li>
    );
  }
}
