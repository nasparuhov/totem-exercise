import React from 'react';
import { shallow, configure } from 'enzyme';
import Adapter from 'enzyme-adapter-react-15.4';
import PaginationComponent from './Pagination.jsx';
import { Pagination } from 'react-bootstrap';

configure({ adapter: new Adapter() });

describe('Pagination Component rendering with 10 pages', () => {
  let pagination = shallow(<PaginationComponent totalPages={10} />);

  it('should render Pagination component without errors', () => {
    expect(pagination.exists()).toBe(true)
  })

  it('Pagination with 10 pages inside + previous and next buttons', () => {
    expect(pagination.find(Pagination.Prev).length).toBe(1);
    expect(pagination.find(Pagination.Item).length).toBe(10);
    expect(pagination.find(Pagination.Next).length).toBe(1);
  })
})

describe('Pagination Component rendering with more than 10 pages', () => {
  let callback = (page) => {console.log(page)}
  let pagination = shallow(<PaginationComponent totalPages={16} handlePageChange={callback}/>);

  it('should render Pagination component without errors', () => {
    expect(pagination.find(Pagination.First).length).toBe(1);
    expect(pagination.find(Pagination.Prev).length).toBe(1);
    expect(pagination.exists()).toBe(true)
    expect(pagination.find(Pagination.Next).length).toBe(1);
    expect(pagination.find(Pagination.Last).length).toBe(1);
  })

  it('Pagination with 7 pages inside (first, block of 5 and the last one) and  selected page is 7', () => {
    pagination.setState({currentPage: 7})
    expect(pagination.find(Pagination.Item).length).toBe(7);
    expect(pagination.find(Pagination.Ellipsis).length).toBe(2);
  })

  it('Pagination with 7 pages again but selected one is 4', () => {
    pagination.setState({currentPage: 4})
    expect(pagination.find(Pagination.Item).length).toBe(7);
    expect(pagination.find(Pagination.Ellipsis).length).toBe(1);
  })

  it('Pagination with 6 pages and selected one is 2', () => {
    pagination.setState({currentPage: 2})
    expect(pagination.find(Pagination.Item).length).toBe(6);
    expect(pagination.find(Pagination.Ellipsis).length).toBe(1);
  })


  it('Pagination with 6 pages and selected one is 14 (n-2)', () => {
    pagination.setState({currentPage: 14})
    expect(pagination.find(Pagination.Item).length).toBe(6);
    expect(pagination.find(Pagination.Ellipsis).length).toBe(1);
  })

  it('should change the current page to 4', () => {
    pagination.setState({currentPage: 3})
    expect(pagination.find(Pagination.Item).length).toBe(6);
    pagination.find(Pagination.Item).at(4).simulate('click');
    expect(pagination.state().currentPage).toBe(5)
    expect(pagination.find(Pagination.Item).length).toBe(7);
    expect(pagination.find(Pagination.Ellipsis).length).toBe(2);
  })
})
