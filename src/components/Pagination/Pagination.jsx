import React from 'react';
import PropTypes from 'prop-types';
import { Pagination } from 'react-bootstrap';

export default class PaginationComponent extends React.Component {
  static propTypes = {
    totalPages: PropTypes.number,
    handlePageChange: PropTypes.func
  }

  static defaultProps = {
    totalPages: 1
  }

  constructor(props) {
    super(props);
    this.state = {
      currentPage: 1,
      totalPages: this.props.totalPages,
      visiblePages: []
    };
  }

  handlePageClick = index => {
    if (this.state.currentPage !== index && index > 0 && index < this.props.totalPages + 1) {
      this.setState({ currentPage: index });
      this.props.handlePageChange(index);
    }
  }

  renderPagination = () => {
    if (this.props.totalPages <= 10) {
      return (
        <Pagination>
          <Pagination.Prev onClick={this.handlePageClick.bind(this, this.state.currentPage - 1)} disabled={this.state.currentPage === 1} />
          {this.renderPages()}
          <Pagination.Next onClick={this.handlePageClick.bind(this, this.state.currentPage + 1)}
            disabled={this.state.currentPage === this.props.totalPages} />
        </Pagination>
      );
    } else {
      return (
          <Pagination>
            <Pagination.First onClick={this.handlePageClick.bind(this, 1)} disabled={this.state.currentPage === 1} />
            <Pagination.Prev onClick={this.handlePageClick.bind(this, this.state.currentPage - 1)} disabled={this.state.currentPage === 1} />
            {this.state.currentPage > 3 ? <Pagination.Item active={this.state.currentPage === 1}
              onClick={this.handlePageClick.bind(this, 1)}>{1}</Pagination.Item> : ''}
            {this.state.currentPage > 4 ? <Pagination.Ellipsis disabled /> : ''}
            {this.renderPages()}
            {this.state.currentPage < this.props.totalPages - 3 ? <Pagination.Ellipsis disabled /> : ''}
            {this.state.currentPage < this.props.totalPages - 2 ? <Pagination.Item active={this.props.totalPages === this.state.currentPage}
              onClick={this.handlePageClick.bind(this, this.props.totalPages)}>{this.props.totalPages}</Pagination.Item> : ''}
            <Pagination.Next onClick={this.handlePageClick.bind(this, this.state.currentPage + 1)}
              disabled={this.state.currentPage === this.props.totalPages} />
            <Pagination.Last onClick={this.handlePageClick.bind(this, this.props.totalPages)}
              disabled={this.state.currentPage === this.props.totalPages} />
          </Pagination>
      );
    }
  }

  renderPages = () => {
    let pages = [];
    let middle = 0;
    switch (true) {
      case this.state.currentPage <= 3: middle = 3; break;
      case this.state.currentPage > 3 && this.state.currentPage <= this.props.totalPages - 3: middle = this.state.currentPage; break;
      case this.state.currentPage > this.props.totalPages - 3: middle = this.props.totalPages - 2; break;
      default: middle = 3; break;
    }
    if (this.props.totalPages > 10) {
      for (let number = middle - 2; number <= middle + 2; number++) {
        pages.push(
          <Pagination.Item active={number === this.state.currentPage} key={number}
            onClick={this.handlePageClick.bind(this, number)}>{number}</Pagination.Item>
        );
      }
    } else {
      for (let number = 1; number <= this.props.totalPages; number++) {
        pages.push(
          <Pagination.Item active={number === this.state.currentPage} key={number}
            onClick={this.handlePageClick.bind(this, number)}>{number}</Pagination.Item>
        );
      }
    }
    return pages;
  }

  render() {
    return (
      <div className='container text-center' style={this.props.totalPages > 1 ? { display: 'block' } : { display: 'none' }}>
        {this.renderPagination()}
      </div>
    );
  }
}
